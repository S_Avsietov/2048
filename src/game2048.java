import controllers.GameController;
import javafx.application.Application;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class game2048 extends Application {
    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        GameController gameController = new GameController(primaryStage);
        primaryStage.setScene(gameController.getScene());
        primaryStage.setTitle("2048");
        primaryStage.getIcons().add(new Image("/resources/2048-icon.png"));
        primaryStage.show();
    }
}
