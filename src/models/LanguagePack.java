package models;

import javafx.beans.binding.StringBinding;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

import java.util.Locale;
import java.util.ResourceBundle;

public class LanguagePack {
    private static ObjectProperty<ResourceBundle> langInstance;
    private static final String LANG_PACK_NAME = "resources.LanguagePack";
    private static int langNumber = getDefaultLanguage().ordinal();

    private LanguagePack() {
    }

    /**
     * Lazy getter for ResourceBundle property
     *
     * @return ResourceBundle property with language pack
     */
    public static ObjectProperty<ResourceBundle> langProperty() {
        if (langInstance == null) {
            langInstance = new SimpleObjectProperty<>();
            setLangBundle(getDefaultLanguage());
        }
        return langInstance;
    }

    private static Language getDefaultLanguage() {
        Language defaultLanguage;
        try {
            defaultLanguage = Language.valueOf(Locale.getDefault().getCountry());
        } catch (IllegalArgumentException e) {
            defaultLanguage = Language.EN;
        }
        return defaultLanguage;
    }

    /**
     * Getter for ResourceBundle language pack
     *
     * @return ResourceBundle with language pack
     */
    public static ResourceBundle getLangBundle() {
        return langProperty().get();
    }

    /**
     * Setter for ResourceBundle for specified language
     *
     * @param language
     */
    public static void setLangBundle(Language language) {
        langProperty()
                .set(ResourceBundle
                        .getBundle(LANG_PACK_NAME
                                , Locale.forLanguageTag(language.toString())));
    }

    /**
     * Setter who changes ResourceBundle for language in a circle path
     */
    public static void setNextLangBundle() {
        if (++langNumber >= Language.values().length)
            langNumber = 0;
        setLangBundle(Language.values()[langNumber]);
    }

    /**
     * StringBinding factory binding node text property with property in ResourceBundle language pack
     *
     * @param key property in ResourceBundle language pack
     * @return
     */
    public static StringBinding stringBinding(String key) {
        return new StringBinding() {
            {
                this.bind(langProperty());
            }

            @Override
            protected String computeValue() {
                return getLangBundle().getString(key);
            }
        };
    }
}
