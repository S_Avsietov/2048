package models;

import java.io.Serializable;
import java.time.LocalDateTime;

public class Score implements Comparable<Score>, Serializable {
    private static final LocalDateTime DEFAULT_DATE_TIME = LocalDateTime.now();
    private static final int DEFAULT_PLAYER_SCORE = 0;
    /**
     * Player's high score date
     */
    LocalDateTime dateTime;

    /**
     * Player's high score
     */
    int playerScore;

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public int getPlayerScore() {
        return playerScore;
    }

    public Score() {
        this(DEFAULT_DATE_TIME, DEFAULT_PLAYER_SCORE);
    }

    public Score(LocalDateTime dateTime, int playerScore) {
        this.dateTime = dateTime;
        this.playerScore = playerScore;
    }

    public Score(int playerScore) {
        this.playerScore = playerScore;
        this.dateTime = LocalDateTime.now();
    }

    @Override
    public int compareTo(Score score) {
        return this.playerScore - score.playerScore;
    }
}
