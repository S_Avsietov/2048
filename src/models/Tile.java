package models;


import java.io.Serializable;

/**
 * A class represents tiles on a game board
 */
public class Tile implements Serializable {
    /**
     * Tile's value
     */
    int value;

    /**
     * Constructor for any Tile
     *
     * @param value
     */
    public Tile(int value) {
        this.value = value;
    }

    /**
     * Constructor for empty Tile
     */
    public Tile() {
        this(0);
    }

    /**
     * Method checking that the tile is not empty
     *
     * @return tile with a value of 0 is considered empty (true)
     */
    public boolean isEmpty() {
        return value == 0;
    }

    /**
     * Getter for tile's value
     *
     * @return tile's value
     */
    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }
}
