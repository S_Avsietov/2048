package models;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.*;
import java.util.stream.IntStream;

public class GameModel implements Serializable {
    private static final int WIN_TILE_VALUE = 2048;
    /**
     * The number of tiles vertically and horizontally
     */
    private static final int TILE_NUMBER = 4;
    /**
     * All game tiles
     */
    private Tile[][] gameTiles;
    /**
     * Current game score
     */
    private transient IntegerProperty score;
    /**
     * Max tile value
     */
    private int maxTile;
    /**
     * Previous states of the game tiles
     */
    private Deque<Tile[][]> previousStates;
    /**
     * Previous game score
     */
    private Deque<Integer> previousScores;

    /**
     * Constructor for GameModel
     */
    public GameModel() {
        gameTiles = new Tile[TILE_NUMBER][TILE_NUMBER];
        score = new SimpleIntegerProperty(0);
        resetGameModel();
    }

    /**
     * Getter for gameTiles
     *
     * @return game Tiles
     */
    public Tile[][] getGameTiles() {
        return gameTiles;
    }

    /**
     * Get list of empty tiles(value=0)
     *
     * @return list of empty tiles
     */
    private List<Tile> getEmptyTiles() {
        List<Tile> emptyTiles = new ArrayList<>();
        for (Tile[] tiles : gameTiles)
            for (Tile tile : tiles)
                if (tile.isEmpty())
                    emptyTiles.add(tile);
        return emptyTiles;
    }

    /**
     * Add new random tile to model
     */
    private void addTile() {
        List<Tile> emptyTiles = getEmptyTiles();
        if (emptyTiles != null && emptyTiles.size() != 0) {
            Tile randomEmptyTile = emptyTiles.get((int) (emptyTiles.size() * Math.random()));
            randomEmptyTile.value = Math.random() < 0.9 ? 2 : 4;
        }
    }

    /**
     * Restart game method
     */
    public void resetGameModel() {
        Arrays.stream(gameTiles).forEach(tiles -> Arrays.setAll(tiles, i -> new Tile()));
        previousStates = new LinkedList<>();
        previousScores = new LinkedList<>();
        score.setValue(0);
        maxTile = 0;
        addTile();
        addTile();
    }

    /**
     * Copy non empty tiles to empty tiles
     *
     * @param tiles
     * @return
     */
    private boolean compressTiles(Tile[] tiles) {
        boolean isTilesStateChanged = false;
        int lastZeroElementIndex = 0;
        for (int i = 0; i < TILE_NUMBER; i++)
            if (tiles[i].value != 0) {
                if (lastZeroElementIndex != i) isTilesStateChanged = true;
                tiles[lastZeroElementIndex++].value = tiles[i].value;
            }

        for (int i = lastZeroElementIndex; i < TILE_NUMBER; i++)
            tiles[i].value = 0;

        return isTilesStateChanged;
    }

    /**
     * Merging adjacent identical tiles
     *
     * @param tiles
     * @return
     */
    private boolean mergeTiles(Tile[] tiles) {
        boolean isTilesStateChanged = false;
        for (int i = 0; i < TILE_NUMBER - 1; i++) {
            if (tiles[i].value != 0 && tiles[i].value == tiles[i + 1].value) {
                tiles[i].value += tiles[i + 1].value;
                tiles[i + 1].value = 0;
                score.setValue(score.getValue() + tiles[i].value);
                if (tiles[i].value > maxTile)
                    maxTile = tiles[i].value;
                isTilesStateChanged = true;
            }
        }
        if (isTilesStateChanged)
            compressTiles(tiles);
        return isTilesStateChanged;
    }

    /**
     * Left move
     */
    public void shiftLeft() {
        boolean isTilesStateChanged = false;
        for (Tile[] tiles : gameTiles) {
            if (compressTiles(tiles)) isTilesStateChanged = true;
            if (mergeTiles(tiles)) isTilesStateChanged = true;
        }
        if (isTilesStateChanged)
            addTile();
    }

    /**
     * Rotate matrix to 90 degree
     */
    private void rotate() {
        Tile buffer;
        int iMax = TILE_NUMBER / 2;
        int jMax = TILE_NUMBER - 1;
        for (int i = 0; i < iMax; i++)
            for (int j = i; j < jMax - i; j++) {
                buffer = gameTiles[i][j];
                gameTiles[i][j] = gameTiles[jMax - j][i];
                gameTiles[jMax - j][i] = gameTiles[jMax - i][jMax - j];
                gameTiles[jMax - i][jMax - j] = gameTiles[j][jMax - i];
                gameTiles[j][jMax - i] = buffer;
            }
    }

    /**
     * Left move
     */
    public void left() {
        saveState();
        shiftLeft();
    }

    /**
     * Move down
     */
    public void down() {
        saveState();
        rotate();
        shiftLeft();
        rotate();
        rotate();
        rotate();
    }

    /**
     * Move up
     */
    public void up() {
        saveState();
        rotate();
        rotate();
        rotate();
        shiftLeft();
        rotate();
    }

    /**
     * Move right
     */
    public void right() {
        saveState();
        rotate();
        rotate();
        shiftLeft();
        rotate();
        rotate();
    }

    /**
     * Method returning a model property with current game score
     *
     * @return
     */
    public ReadOnlyIntegerProperty scoreProperty() {
        return score;
    }

    /**
     * Method for serializing a class whose field does not support serialization
     *
     * @param s stream into which the object is serialized
     * @throws IOException
     */
    private void writeObject(ObjectOutputStream s) throws IOException {
        s.defaultWriteObject();
        s.writeInt(score.getValue());
    }

    /**
     * Method for deserializing a class whose field does not support serialization
     *
     * @param s stream from which the object is deserialized
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private void readObject(ObjectInputStream s) throws IOException, ClassNotFoundException {
        s.defaultReadObject();
        score = new SimpleIntegerProperty(s.readInt());
    }

    /**
     * Rollback to previous game state
     */
    public void rollback() {
        if (previousStates.peek() != null && previousScores.peek() != null) {
            gameTiles = previousStates.pop();
            score.setValue(previousScores.pop());
        }
    }

    /**
     * Save game state
     */
    public void saveState() {
        if (!hasStateChanged())
            return;

        Tile[][] previousGameTiles = new Tile[TILE_NUMBER][TILE_NUMBER];
        for (int i = 0; i < TILE_NUMBER; i++)
            for (int j = 0; j < TILE_NUMBER; j++)
                previousGameTiles[i][j] = new Tile(gameTiles[i][j].value);
        previousStates.push(previousGameTiles);
        previousScores.push(score.getValue());
    }

    /**
     * Checking that the state has changed after the move
     *
     * @return true if changed
     */
    private boolean hasStateChanged() {
        if (previousStates.peek() == null)
            return true;

        int tilesWeight = Arrays
                .stream(gameTiles)
                .flatMap(Arrays::stream)
                .flatMapToInt(tile -> IntStream.of(tile.value))
                .sum();
        int prevTilesWeight = Arrays
                .stream(previousStates.peek())
                .flatMap(Arrays::stream)
                .flatMapToInt(tile -> IntStream.of(tile.value))
                .sum();
        return tilesWeight != prevTilesWeight;
    }

    /**
     * Method of checking the possibility of a move in the game
     *
     * @return true if there is a move
     */
    public boolean canMove() {
        for (int i = 0; i < TILE_NUMBER; i++)
            for (int j = 0; j < TILE_NUMBER; j++)
                if (i == TILE_NUMBER - 1 && j < TILE_NUMBER - 1) {
                    if (gameTiles[i][j].value == 0 || gameTiles[i][j].value == gameTiles[i][j + 1].value)
                        return true;
                } else if (i < TILE_NUMBER - 1 && j == TILE_NUMBER - 1) {
                    if (gameTiles[i][j].value == 0 || gameTiles[i][j].value == gameTiles[i + 1][j].value)
                        return true;
                } else if (i == TILE_NUMBER - 1 && j == TILE_NUMBER - 1) {
                    if (gameTiles[i][j].value == 0) return true;
                } else {
                    if (gameTiles[i][j].value == 0
                            || gameTiles[i][j + 1].value == 0
                            || gameTiles[i + 1][j].value == 0
                            || gameTiles[i][j].value == gameTiles[i][j + 1].value
                            || gameTiles[i][j].value == gameTiles[i + 1][j].value)
                        return true;
                }
        return false;
    }

    public boolean isWin() {
        return maxTile >= WIN_TILE_VALUE;
    }

}
