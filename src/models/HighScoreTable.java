package models;

import utils.Helper;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class HighScoreTable implements Serializable {
    private static HighScoreTable instance;
    /**
     * Capacity of top N player's result table
     */
    private static final int HIGH_SCORE_TABLE_CAPACITY = 10;

    /**
     * top N player's result table
     */
    private Score[] highScores;

    public Score[] getHighScores() {
        return highScores;
    }

    private HighScoreTable() {
        this.highScores = new Score[HIGH_SCORE_TABLE_CAPACITY];
        Arrays.fill(this.highScores, new Score());
    }

    public static HighScoreTable getInstance() throws IOException, ClassNotFoundException {
        if (instance == null)
            load();
        return instance;
    }

    private static void load() throws IOException, ClassNotFoundException {
        Path defaultDir = Helper.getGameDefaultDir();
        Path highScoreTableFile = defaultDir.resolve("leaderbord.bin");
        if (Files.exists(highScoreTableFile))
            try (FileInputStream fileInputStream = new FileInputStream(highScoreTableFile.toFile());
                 ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream)) {
                instance = (HighScoreTable) objectInputStream.readObject();
            }
        else
            instance = new HighScoreTable();
    }

    private static void save() throws IOException {
        Path defaultDir = Helper.getGameDefaultDir();
        Path highScoreTableFile = defaultDir.resolve("leaderbord.bin");
        try (FileOutputStream fileOutputStream = new FileOutputStream(highScoreTableFile.toFile());
             ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream)) {
            objectOutputStream.writeObject(instance);
        }
    }

    /**
     * Check if score value is new record
     *
     * @param score
     * @return
     */
    private boolean isNewRecord(int score) {
        boolean isSameScore = Arrays.
                stream(highScores).anyMatch(s -> s.playerScore == score);
        if (!isSameScore) {
            int minScore = Arrays
                    .stream(highScores)
                    .min(Score::compareTo)
                    .orElseGet(Score::new)
                    .playerScore;
            return score > minScore;
        }
        return false;
    }

    /**
     * Add new best score to high score table
     *
     * @param score new best score
     */
    public void addScore(Score score) throws IOException {
        if (isNewRecord(score.playerScore)) {
            List<Score> scoreList = new ArrayList<>();
            Collections.addAll(scoreList, highScores);
            scoreList.add(score);
            highScores = scoreList
                    .stream()
                    .sorted((score1, score2) -> score2.playerScore - score1.playerScore)
                    .limit(HIGH_SCORE_TABLE_CAPACITY)
                    .toArray(Score[]::new);
        }
        save();
    }
}
