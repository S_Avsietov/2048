package utils;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import models.LanguagePack;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Helper {
    /**
     * Style for ScorePanel's grid
     */
    public static final String PANEL_STYLE = "-fx-padding: 15 15 15 15;" +
            "-fx-background-color: #BBADA0;" +
            "-fx-hgap: 10;" +
            "-fx-vgap: 10;";
    /**
     * Style for ScorePanel's grid
     */
    public static final String PANEL_STYLE_WITH_NO_BACKGROUND = "-fx-padding: 15 15 15 15;" +
            "-fx-hgap: 10;" +
            "-fx-vgap: 10;";
    /**
     * Style for buttons
     */
    public static final String BUTTON_STYLE = "-fx-padding: 8 15 15 15;" +
            "-fx-background-insets: 0,0 0 5 0, 0 0 6 0, 0 0 7 0;" +
            "-fx-background-radius: 8;" +
            "-fx-background-color: #dbb399;" +
            "-fx-effect: dropshadow( gaussian , rgba(0,0,0,0.75) , 14,0,0,1 );" +
            "-fx-font-family: \"Arial\";" +
            "-fx-font-weight: bold;" +
            "-fx-text-fill: #776e65;" +
            "-fx-font-size: 18px;";
    /**
     * Style for label
     */
    public static final String LABEL_STYLE = "-fx-padding: 8 15 15 15;" +
            "-fx-background-insets: 0,0 0 5 0, 0 0 6 0, 0 0 7 0;" +
            "-fx-background-radius: 8;" +
            "-fx-background-color: #eebe07;" +
            "-fx-effect: dropshadow( gaussian , rgba(0,0,0,0.75) , 14,0,0,1 );" +
            "-fx-font-family: \"Arial\";" +
            "-fx-font-weight: bold;" +
            "-fx-text-fill: #776e65;" +
            "-fx-font-size: 36px;";

    /**
     * Style for label
     */
    public static final String SMALL_LABEL_STYLE = "-fx-padding: 8 15 15 15;" +
            "-fx-background-insets: 0,0 0 5 0, 0 0 6 0, 0 0 7 0;" +
            "-fx-background-radius: 8;" +
            "-fx-background-color: #eebe07;" +
            "-fx-effect: dropshadow( gaussian , rgba(0,0,0,0.75) , 14,0,0,1 );" +
            "-fx-font-family: \"Arial\";" +
            "-fx-font-weight: bold;" +
            "-fx-text-fill: #776e65;" +
            "-fx-font-size: 18px;";


    /**
     * Creation method for button
     *
     * @param text button text
     * @return Button
     */
    public static Button createButton(String text) {
        Button button = new Button(text);
        button.setStyle(BUTTON_STYLE);
        button.setAlignment(Pos.CENTER);
        button.setMaxWidth(Double.MAX_VALUE);
        return button;
    }

    /**
     * Creation method for button
     *
     * @param bindingKey key in resource file from text property
     * @return Button with binded  to resource file text property
     */
    public static Button createBindedButton(String bindingKey) {
        Button button = createButton("");
        button.textProperty().bind(LanguagePack.stringBinding(bindingKey));
        return button;
    }

    /**
     * Creation method for label
     *
     * @param text label text
     * @return Label
     */
    public static Label createSmallLabel(String text) {
        Label label = new Label(text);
        label.setAlignment(Pos.CENTER);
        label.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        label.setStyle(SMALL_LABEL_STYLE);
        return label;
    }

    /**
     * Creation method for label
     *
     * @param text label text
     * @return Label
     */
    public static Label createLabel(String text) {
        Label label = new Label(text);
        label.setAlignment(Pos.CENTER);
        label.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        label.setStyle(LABEL_STYLE);
        return label;
    }

    /**
     * Creation method for label
     *
     * @param bindingKey key in resource file from text property
     * @return Label with binded to resource file text property
     */
    public static Label createBindedLabel(String bindingKey) {
        Label label = createLabel("");
        label.textProperty().bind(LanguagePack.stringBinding(bindingKey));
        return label;
    }

    /**
     * Method for creating columns and rows on the GridPane
     *
     * @param grid    GridPane
     * @param columns Array with column sizes in percent
     * @param rows    Array with row sizes in percent
     */
    public static void markupGrid(GridPane grid, int[] columns, int[] rows) {
        for (int percentage : columns) {
            ColumnConstraints columnConstraints = new ColumnConstraints();
            columnConstraints.setPercentWidth(percentage);
            grid.getColumnConstraints().add(columnConstraints);
        }

        for (int percentage : rows) {
            RowConstraints rowConstraints = new RowConstraints();
            rowConstraints.setPercentHeight(percentage);
            grid.getRowConstraints().add(rowConstraints);
        }
    }

    public static Path getGameDefaultDir() throws IOException {
        Path defaultDir = Paths.get(System.getProperty("user.home") + "/2048");
        if (Files.notExists(defaultDir))
            Files.createDirectory(defaultDir);
        return defaultDir;
    }

}
