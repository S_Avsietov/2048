package controllers.commands;

import controllers.GameController;
import controllers.commands.game.*;
import controllers.commands.gui.*;
import javafx.scene.Scene;

public class CommandFactory {
    private GameController controller;
    private Scene parentScene;

    public CommandFactory(GameController controller, Scene parentScene) {
        this.controller = controller;
        this.parentScene = parentScene;
    }

    public CommandGame create(GameCommand gameCommand) {
        CommandGame command;
        switch (gameCommand) {
            case Down:
                command = new MoveDownCommand(controller);
                break;
            case Left:
                command = new MoveLeftCommand(controller);
                break;
            case Right:
                command = new MoveRightCommand(controller);
                break;
            case Up:
                command = new MoveUpCommand(controller);
                break;
            case Restart:
                command = new RestartCommand(controller);
                break;
            case Rollback:
                command = new RollbackCommand(controller);
                break;
            default:
                command = null;
        }
        return command;
    }

    public CommandGUI create(GUICommand guiCommand) {
        CommandGUI command;
        switch (guiCommand) {
            case Back:
                command = new BackToParentSceneCommand(controller, parentScene);
                break;
            case Menu:
                command = new ShowMenuCommand(controller, parentScene);
                break;
            case Save:
                command = new SaveGameCommand(controller);
                break;
            case Load:
                command = new LoadGameCommand(controller);
                break;
            case ChangeLanguage:
                command = new ChangeLanguageCommand();
                break;
            case HighScoreTable:
                command = new ShowHighScoreTableCommand(controller, parentScene);
                break;
            default:
                command = null;
        }
        return command;
    }
}
