package controllers.commands.gui;

import controllers.GameController;
import controllers.MenuController;
import javafx.scene.Scene;

public class ShowMenuCommand extends AbstractGUICommand {

    public ShowMenuCommand(GameController controller, Scene parentScene) {
        super(controller, parentScene);
    }

    @Override
    public void execute() {
        new MenuController(controller, parentScene);
    }
}
