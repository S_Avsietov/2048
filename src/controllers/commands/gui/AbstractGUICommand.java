package controllers.commands.gui;

import controllers.GameController;
import javafx.scene.Scene;

import java.io.IOException;

public abstract class AbstractGUICommand implements CommandGUI {
    protected GameController controller;
    protected Scene parentScene;

    public AbstractGUICommand() {
    }

    public AbstractGUICommand(GameController controller) {
        this.controller = controller;
    }

    public AbstractGUICommand(GameController controller, Scene parentScene) {
        this.controller = controller;
        this.parentScene = parentScene;
    }

    @Override
    public abstract void execute() throws IOException, ClassNotFoundException;
}
