package controllers.commands.gui;

import controllers.GameController;
import javafx.stage.FileChooser;
import models.GameModel;
import models.LanguagePack;
import utils.Helper;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.nio.file.Path;

public class LoadGameCommand extends AbstractGUICommand {

    public LoadGameCommand(GameController controller) {
        super(controller);
    }

    @Override
    public void execute() throws IOException, ClassNotFoundException {
        FileChooser fileChooser = new FileChooser();
        Path defaultDir = Helper.getGameDefaultDir();
        fileChooser.titleProperty().bind(LanguagePack.stringBinding("button.loadGame"));
        fileChooser.setInitialDirectory(defaultDir.toFile());
        fileChooser.getExtensionFilters()
                .add(new FileChooser.ExtensionFilter("Game saves (*.sav)", "*.sav"));

        File inputFIle = fileChooser.showOpenDialog(controller.getStage());
        if (inputFIle != null)
            try (FileInputStream fileInputStream = new FileInputStream(inputFIle);
                 ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream)) {
                GameModel model = (GameModel) objectInputStream.readObject();
                controller.setModel(model);
                controller.getBoard().draw();
            }
    }
}
