package controllers.commands.gui;

import java.io.IOException;

public interface CommandGUI {
    void execute() throws IOException, ClassNotFoundException;
}
