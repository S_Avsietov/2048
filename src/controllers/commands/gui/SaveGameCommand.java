package controllers.commands.gui;

import controllers.GameController;
import javafx.stage.FileChooser;
import models.LanguagePack;
import utils.Helper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.file.Path;

public class SaveGameCommand extends AbstractGUICommand {

    public SaveGameCommand(GameController controller) {
        super(controller);
    }

    @Override
    public void execute() throws IOException {
        FileChooser fileChooser = new FileChooser();
        Path defaultDir = Helper.getGameDefaultDir();
        fileChooser.titleProperty().bind(LanguagePack.stringBinding("button.saveGame"));
        fileChooser.setInitialDirectory(defaultDir.toFile());
        fileChooser.getExtensionFilters()
                .add(new FileChooser.ExtensionFilter("Game saves (*.sav)", "*.sav"));

        File outputFile = fileChooser.showSaveDialog(controller.getStage());
        if (outputFile != null)
            try (FileOutputStream fileOutputStream = new FileOutputStream(outputFile);
                 ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream)) {
                objectOutputStream.writeObject(controller.getModel());
            }
    }
}
