package controllers.commands.gui;

import controllers.GameController;
import controllers.HighScoreTableController;
import javafx.scene.Scene;

import java.io.IOException;

public class ShowHighScoreTableCommand extends AbstractGUICommand {

    public ShowHighScoreTableCommand(GameController controller, Scene parentScene) {
        super(controller, parentScene);
    }

    @Override
    public void execute() throws IOException, ClassNotFoundException {
        new HighScoreTableController(controller, parentScene);
    }
}
