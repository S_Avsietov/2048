package controllers.commands.gui;

public enum GUICommand {
    Back, Menu, Save, Load, ChangeLanguage, HighScoreTable
}
