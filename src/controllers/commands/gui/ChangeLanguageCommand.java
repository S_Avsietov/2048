package controllers.commands.gui;

import models.LanguagePack;

public class ChangeLanguageCommand extends AbstractGUICommand {

    @Override
    public void execute() {
        LanguagePack.setNextLangBundle();
    }
}
