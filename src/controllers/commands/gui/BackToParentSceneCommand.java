package controllers.commands.gui;

import controllers.GameController;
import javafx.scene.Scene;

public class BackToParentSceneCommand extends AbstractGUICommand {

    public BackToParentSceneCommand(GameController controller, Scene parentScene) {
        super(controller, parentScene);
    }

    @Override
    public void execute() {
        controller.getStage().setScene(parentScene);
        controller.getStage().show();
    }
}
