package controllers.commands.game;

import controllers.GameController;

public class RestartCommand extends AbstractGameCommand {

    public RestartCommand(GameController controller) {
        super(controller);
    }

    @Override
    public void execute() {
        controller.getModel().resetGameModel();
        controller.getBoard().draw();
    }
}
