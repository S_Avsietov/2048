package controllers.commands.game;

import controllers.GameController;

public class MoveLeftCommand extends AbstractGameCommand {

    public MoveLeftCommand(GameController controller) {
        super(controller);
    }

    @Override
    public void execute() {
        controller.getModel().left();
        controller.getBoard().draw();
    }
}
