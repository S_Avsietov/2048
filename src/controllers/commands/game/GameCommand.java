package controllers.commands.game;

public enum GameCommand {
    Down, Left, Right, Up, Restart, Rollback
}
