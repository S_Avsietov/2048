package controllers.commands.game;

import controllers.GameController;

public class MoveRightCommand extends AbstractGameCommand {

    public MoveRightCommand(GameController controller) {
        super(controller);
    }

    @Override
    public void execute() {
        controller.getModel().right();
        controller.getBoard().draw();
    }
}
