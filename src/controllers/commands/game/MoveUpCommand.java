package controllers.commands.game;

import controllers.GameController;

public class MoveUpCommand extends AbstractGameCommand {

    public MoveUpCommand(GameController controller) {
        super(controller);
    }

    @Override
    public void execute() {
        controller.getModel().up();
        controller.getBoard().draw();
    }
}
