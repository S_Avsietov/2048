package controllers.commands.game;

import controllers.GameController;

public class RollbackCommand extends AbstractGameCommand {

    public RollbackCommand(GameController controller) {
        super(controller);
    }

    @Override
    public void execute() {
        controller.getModel().rollback();
        controller.getBoard().draw();
    }
}
