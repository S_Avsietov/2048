package controllers.commands.game;

import controllers.GameController;

public class MoveDownCommand extends AbstractGameCommand {

    public MoveDownCommand(GameController controller) {
        super(controller);
    }

    @Override
    public void execute() {
        controller.getModel().down();
        controller.getBoard().draw();
    }
}
