package controllers.commands.game;

import controllers.GameController;

public abstract class AbstractGameCommand implements CommandGame {
    protected GameController controller;

    public AbstractGameCommand(GameController controller) {
        this.controller = controller;
    }

    @Override
    public abstract void execute();
}
