package controllers;

import controllers.commands.CommandFactory;
import controllers.commands.gui.GUICommand;
import controllers.commands.game.GameCommand;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import models.GameModel;
import models.HighScoreTable;
import models.Score;
import models.Tile;
import views.Board;
import views.ButtonPanel;
import views.ScorePanel;

import java.io.IOException;

public class GameController {
    /**
     * Reference to game model
     */
    private GameModel model;
    /**
     * Reference to game board
     */
    private Board board;
    /**
     * Reference to button panel
     */
    private ButtonPanel buttonPanel;
    /**
     * Reference to score panel
     */
    private ScorePanel scorePanel;
    /**
     * Reference to the scene containing board, button panel, score panel
     */
    private Scene scene;
    /**
     * Reference to the main stage
     */
    private Stage stage;
    /**
     * Reference to controller command factory
     */
    private CommandFactory commandFactory;

    public GameController(Stage stage) {
        this.model = new GameModel();
        this.stage = stage;
        createScene();
        this.commandFactory = new CommandFactory(this, scene);
        setSceneHandler();
        setButtonPanelHandler();
        setScorePanelHandler();
    }

    /**
     * Method for creating scene with all elements (board, button panel, score panel)
     */
    private void createScene() {
        board = new Board(this);
        buttonPanel = new ButtonPanel();
        scorePanel = new ScorePanel();
        BorderPane root = new BorderPane();
        root.setCenter(board);
        root.setTop(scorePanel);
        root.setBottom(buttonPanel);
        scene = new Scene(root, 470, 750);
    }

    /**
     * Getter for game Tiles from model
     *
     * @return game Tiles
     */
    public Tile[][] getGameTiles() {
        return model.getGameTiles();
    }

    /**
     * Getter for scene
     *
     * @return scene containing board, button panel, score panel
     */
    public Scene getScene() {
        return scene;
    }

    /**
     * Getter for game model
     *
     * @return game model
     */
    public GameModel getModel() {
        return model;
    }

    /**
     * Getter for board
     *
     * @return board
     */
    public Board getBoard() {
        return board;
    }

    /**
     * Getter for stage
     *
     * @return main stage
     */
    public Stage getStage() {
        return stage;
    }

    /**
     * Method for link model's score value with score panel
     */
    private void setScorePanelHandler() {
        model.scoreProperty()
                .addListener((observable, oldValue, newValue) ->
                        scorePanel.setScore(newValue.toString()));
        this.scorePanel.setScore(model.scoreProperty().getValue().toString());
    }

    /**
     * Setter for game model
     *
     * @param model
     */
    public void setModel(GameModel model) {
        this.model = model;
        setScorePanelHandler();
    }

    private void saveScore() {
        if(!model.canMove()) {
            try {
                HighScoreTable.getInstance().addScore(new Score(model.scoreProperty().getValue()));
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Method to set the handler for button pane
     */
    private void setButtonPanelHandler() {
        buttonPanel.setOnDownButtonAction(event -> {
            commandFactory.create(GameCommand.Down).execute();
            saveScore();
        });
        buttonPanel.setOnUpButtonAction(event -> {
            commandFactory.create(GameCommand.Up).execute();
            saveScore();
        });
        buttonPanel.setOnRightButtonAction(event -> {
            commandFactory.create(GameCommand.Right).execute();
            saveScore();
        });
        buttonPanel.setOnLeftButtonAction(event -> {
            commandFactory.create(GameCommand.Left).execute();
            saveScore();
        });
        buttonPanel.setOnMenuButtonAction(event -> {
            try {
                commandFactory.create(GUICommand.Menu).execute();
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        });
        buttonPanel.setOnRollbackButtonAction(event -> {
            commandFactory.create(GameCommand.Rollback).execute();
        });
    }

    /**
     * Method to set the handler for scene
     */
    private void setSceneHandler() {
        scene.setOnKeyPressed(event -> {
            switch (event.getCode()) {
                case LEFT:
                    commandFactory.create(GameCommand.Left).execute();
                    saveScore();
                    break;
                case RIGHT:
                    commandFactory.create(GameCommand.Right).execute();
                    saveScore();
                    break;
                case DOWN:
                    commandFactory.create(GameCommand.Down).execute();
                    saveScore();
                    break;
                case UP:
                    commandFactory.create(GameCommand.Up).execute();
                    saveScore();
                    break;
                case R:
                    commandFactory.create(GameCommand.Rollback).execute();
                    break;
            }
        });
    }
}
