package controllers;

import controllers.commands.CommandFactory;
import controllers.commands.gui.GUICommand;
import controllers.commands.game.GameCommand;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import views.BackButtonPanel;
import views.MenuPanel;

import java.io.IOException;

public class MenuController {
    /**
     * Reference to back button panel
     */
    private BackButtonPanel backButtonPanel;
    /**
     * Reference to menu panel
     */
    private MenuPanel menuPanel;
    /**
     * Reference to the scene containing menu panel, back button panel
     */
    private Scene scene;
    /**
     * Reference to the main controller
     */
    private GameController controller;
    /**
     * Reference to controller command factory
     */
    private CommandFactory commandFactory;

    public MenuController(GameController controller, Scene parentScene) {
        this.controller = controller;
        this.commandFactory = new CommandFactory(controller, parentScene);
        createScene();
        setBackButtonPanelHandler();
        setMenuPanelHandler();
        controller.getStage().setScene(scene);
        controller.getStage().show();
    }

    /**
     * Creating a scene and arranging visual elements on it
     */
    public void createScene() {
        backButtonPanel = new BackButtonPanel();
        menuPanel = new MenuPanel();
        BorderPane root = new BorderPane();
        root.setCenter(menuPanel);
        root.setBottom(backButtonPanel);
        scene = new Scene(root, controller.getStage().getWidth(), controller.getStage().getHeight());
        controller.getStage().setWidth(scene.getWidth());
        controller.getStage().setHeight(scene.getHeight());
    }

    /**
     * Method for setting the command to the BackPressed button handler
     */
    private void setBackButtonPanelHandler() {
        backButtonPanel.setOnBackPressed(event ->
        {
            try {
                commandFactory.create(GUICommand.Back).execute();
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * Method for setting the command to the RestartPressed button handler
     */
    private void setMenuPanelHandler() {
        menuPanel.setOnNewGameButtonAction(event -> {
            commandFactory.create(GameCommand.Restart).execute();
            try {
                commandFactory.create(GUICommand.Back).execute();
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        });
        menuPanel.setOnSaveButtonAction(event ->
        {
            try {
                commandFactory.create(GUICommand.Save).execute();
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        });
        menuPanel.setOnLoadButtonAction(event -> {
            try {
                commandFactory.create(GUICommand.Load).execute();
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        });
        menuPanel.setOnChangeLangButtonAction(event -> {
            try {
                commandFactory.create(GUICommand.ChangeLanguage).execute();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        });
        menuPanel.setOnHighScoreTableButtonAction(event -> {
            try {
                (new CommandFactory(controller, scene)).create(GUICommand.HighScoreTable).execute();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        });
    }
}
