package controllers;

import controllers.commands.CommandFactory;
import controllers.commands.gui.GUICommand;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import views.BackButtonPanel;
import views.HighScoreTablePanel;

import java.io.IOException;

public class HighScoreTableController {
    /**
     * Reference to back button panel
     */
    private BackButtonPanel backButtonPanel;
    /**
     * Reference to the scene containing button panel, high score table panel
     */
    private Scene scene;
    /**
     * Reference to the main controller
     */
    private final GameController controller;
    /**
     * Reference to controller command factory
     */
    private final CommandFactory commandFactory;

    public HighScoreTableController(GameController controller, Scene parentScene) throws IOException, ClassNotFoundException {
        this.controller = controller;
        this.commandFactory = new CommandFactory(controller, parentScene);
        createScene();
        controller.getStage().setScene(scene);
        controller.getStage().show();
        setBackButtonPanelHandler();
    }

    /**
     * Creating a scene and arranging visual elements on it
     */
    public void createScene() throws IOException, ClassNotFoundException {
        backButtonPanel = new BackButtonPanel();
        HighScoreTablePanel highScoreTablePanel = new HighScoreTablePanel();
        BorderPane root = new BorderPane();
        root.setCenter(highScoreTablePanel);
        root.setBottom(backButtonPanel);
        scene = new Scene(root, controller.getStage().getWidth(), controller.getStage().getHeight());
        controller.getStage().setWidth(scene.getWidth());
        controller.getStage().setHeight(scene.getHeight());
    }

    /**
     * Method for setting the command to the BackPressed button handler
     */
    private void setBackButtonPanelHandler() {
        backButtonPanel.setOnBackPressed(event ->
        {
            try {
                commandFactory.create(GUICommand.Back).execute();
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        });
    }
}
