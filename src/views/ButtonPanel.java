package views;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import utils.Helper;

public class ButtonPanel extends GridPane {
    /**
     * Percentage settings for columns
     */
    private static final int[] COLUMNS = new int[]{33, 34, 33};
    /**
     * Percentage settings for rows
     */
    private static final int[] ROWS = new int[]{33, 34, 33};

    private static final String MOVE_LEFT_BUTTON_KEY = "button.left";
    private static final String MOVE_TOP_BUTTON_KEY = "button.top";
    private static final String MOVE_DOWN_BUTTON_KEY = "button.down";
    private static final String MOVE_RIGHT_BUTTON_KEY = "button.right";
    private static final String MENU_BUTTON_KEY = "button.menu";
    private static final String ROLLBACK_BUTTON_KEY = "button.rollback";

    private Button moveLeftButton;
    private Button moveUpButton;
    private Button moveDownButton;
    private Button moveRightButton;
    private Button menuButton;
    private Button rollbackButton;

    public ButtonPanel() {
        super();
        init();
    }

    /**
     * Initiation method
     */
    private void init() {
        setStyle(Helper.PANEL_STYLE);
        moveLeftButton = Helper.createBindedButton(MOVE_LEFT_BUTTON_KEY);
        moveUpButton = Helper.createBindedButton(MOVE_TOP_BUTTON_KEY);
        moveDownButton = Helper.createBindedButton(MOVE_DOWN_BUTTON_KEY);
        moveRightButton = Helper.createBindedButton(MOVE_RIGHT_BUTTON_KEY);
        menuButton = Helper.createBindedButton(MENU_BUTTON_KEY);
        rollbackButton = Helper.createBindedButton(ROLLBACK_BUTTON_KEY);
        Helper.markupGrid(this, COLUMNS, ROWS);
        add(moveLeftButton, 0, 1, 1, 1);
        add(moveUpButton, 1, 0, 1, 1);
        add(moveDownButton, 1, 2, 1, 1);
        add(moveRightButton, 2, 1, 1, 1);
        add(menuButton, 1, 1, 1, 1);
        add(rollbackButton, 0, 2, 1, 1);
    }

    public void setOnLeftButtonAction(EventHandler<ActionEvent> value) {
        moveLeftButton.setOnAction(value);
    }

    public void setOnRightButtonAction(EventHandler<ActionEvent> value) {
        moveRightButton.setOnAction(value);
    }

    public void setOnUpButtonAction(EventHandler<ActionEvent> value) {
        moveUpButton.setOnAction(value);
    }

    public void setOnDownButtonAction(EventHandler<ActionEvent> value) {
        moveDownButton.setOnAction(value);
    }

    public void setOnMenuButtonAction(EventHandler<ActionEvent> value) {
        menuButton.setOnAction(value);
    }

    public void setOnRollbackButtonAction(EventHandler<ActionEvent> value) {
        rollbackButton.setOnAction(value);
    }
}
