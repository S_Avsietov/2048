package views;

import controllers.GameController;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.geometry.Bounds;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import models.LanguagePack;
import models.Tile;

public class Board extends Canvas {
    /**
     * Board background color
     */
    private static final Color BOARD_COLOR = Color.web("0xBBADA0");
    /**
     * Font name, for text on tiles
     */
    private static final String TILE_FONT_NAME = "Arial";
    /**
     * Minimum tile width
     */
    private static final int MIN_TILE_WIDTH = 80;
    /**
     * Minimum tile height
     */
    private static final int MIN_TILE_HEIGHT = 80;
    /**
     * Tile margin
     */
    private static final int TILE_MARGIN = 10;
    /**
     * The width of the tile for drawing on the board
     */
    private double tileWidth = MIN_TILE_WIDTH;
    /**
     * The height of the tile for drawing on the board
     */
    private double tileHeight = MIN_TILE_HEIGHT;
    /**
     * The number of tiles vertically and horizontally
     */
    private final StringProperty winTextProperty;
    private final StringProperty loseTextProperty;
    private final int TILE_NUMBER;
    private final GameController controller;

    public Board(GameController controller) {
        super();
        this.controller = controller;
        TILE_NUMBER = controller.getGameTiles().length;
        winTextProperty = new SimpleStringProperty();
        loseTextProperty = new SimpleStringProperty();
        winTextProperty.bind(LanguagePack.stringBinding("text.youWin"));
        loseTextProperty.bind(LanguagePack.stringBinding("text.youLose"));
    }

    @Override
    public boolean isResizable() {
        return true;
    }

    @Override
    public double minHeight(double width) {
        return (TILE_NUMBER + 1) * TILE_MARGIN + TILE_NUMBER * MIN_TILE_HEIGHT;
    }

    @Override
    public double maxHeight(double width) {
        return Double.MAX_VALUE;
    }

    @Override
    public double minWidth(double height) {
        return (TILE_NUMBER + 1) * TILE_MARGIN + TILE_NUMBER * MIN_TILE_WIDTH;
    }

    @Override
    public double maxWidth(double height) {
        return Double.MAX_VALUE;
    }

    @Override
    public void resize(double width, double height) {
        setTileHeight(height);
        setTileWidth(width);
        super.setWidth(width);
        super.setHeight(height);
        draw();
    }

    /**
     * Get the color of a tile's text, which depends on its value
     *
     * @param tile game tile
     * @return tile's text color
     */
    private Color getTileTextColor(Tile tile) {
        return tile.getValue() < 16 ? Color.web("0x776e65") : Color.web("0xf9f6f2");
    }

    /**
     * Get the tile's font, which depends on its value
     *
     * @param tile game tile
     * @return tile's font
     */
    private Font getTileFont(Tile tile) {
        int size = tile.getValue() < 100 ? 36 : tile.getValue() < 1000 ? 32 : 24;
        int scalingFactor = (int) (tileWidth < tileHeight ? tileWidth / MIN_TILE_WIDTH : tileHeight / MIN_TILE_HEIGHT);
        return Font.font(TILE_FONT_NAME, FontWeight.BOLD, size * scalingFactor);
    }

    /**
     * Get the color of a tile, which depends on its value
     *
     * @return tile's color
     */
    private Color getTileColor(Tile tile) {
        String colorCode;
        switch (tile.getValue()) {
            case 0:
                colorCode = "0xcdc1b4";
                break;
            case 2:
                colorCode = "0xeee4da";
                break;
            case 4:
                colorCode = "0xede0c8";
                break;
            case 8:
                colorCode = "0xf2b179";
                break;
            case 16:
                colorCode = "0xf59563";
                break;
            case 32:
                colorCode = "0xf67c5f";
                break;
            case 64:
                colorCode = "0xf65e3b";
                break;
            case 128:
                colorCode = "0xedcf72";
                break;
            case 256:
                colorCode = "0xedcc61";
                break;
            case 512:
                colorCode = "0xedc850";
                break;
            case 1024:
                colorCode = "0xedc53f";
                break;
            case 2048:
                colorCode = "0xedc22e";
                break;
            default:
                colorCode = "0xff0000";
        }
        return Color.web(colorCode);
    }

    /**
     * Method for setting the current tile width, which depends on the width of the board
     *
     * @param boardWidth width of the board
     */
    private void setTileWidth(double boardWidth) {
        double newTileWidth = (boardWidth - (TILE_NUMBER + 1) * TILE_MARGIN) / TILE_NUMBER;
        tileWidth = newTileWidth > MIN_TILE_WIDTH ? newTileWidth : MIN_TILE_WIDTH;
    }

    /**
     * Method for setting the current tile height, which depends on the height of the board
     *
     * @param boardHeight height of the board
     */
    private void setTileHeight(double boardHeight) {
        double newTileHeight = (boardHeight - (TILE_NUMBER + 1) * TILE_MARGIN) / TILE_NUMBER;
        tileHeight = newTileHeight > MIN_TILE_HEIGHT ? newTileHeight : MIN_TILE_HEIGHT;
    }

    /**
     * Method for recalculating the tile column index in the upper left tile coordinate on the board
     *
     * @param tileColumnIndex tile column index
     * @return upper left tile X coordinate on the board
     */
    private double translateTileColumnIndexToBoardXCoordinate(int tileColumnIndex) {
        return tileColumnIndex * (tileWidth + TILE_MARGIN) + TILE_MARGIN;
    }

    /**
     * Method for recalculating the tile row index in the upper left tile coordinate on the board
     *
     * @param tileRowIndex tile row index
     * @return upper left tile Y coordinate on the board
     */
    private double translateTileRowIndexToBoardYCoordinate(int tileRowIndex) {
        return tileRowIndex * (tileHeight + TILE_MARGIN) + TILE_MARGIN;
    }

    /**
     * Getting tile text label size
     *
     * @param tile drawable tile
     * @return bounds that is occupied by the inscription on the tile
     */
    private Bounds getTileTextBounds(Tile tile) {
        return getTextBounds(getTileFont(tile), tile.toString());
    }

    private Bounds getTextBounds(Font font, String string) {
        Text text = new Text(string);
        text.setFont(font);
        return text.getBoundsInLocal();
    }

    /**
     * Draw title on board
     *
     * @param tile   drawable tile
     * @param column tile column
     * @param row    tile row
     */
    private void drawTile(Tile tile, int row, int column) {
        GraphicsContext gc = getGraphicsContext2D();
        double x = translateTileColumnIndexToBoardXCoordinate(column);
        double y = translateTileRowIndexToBoardYCoordinate(row);
        gc.setFill(getTileColor(tile));
        gc.fillRoundRect(x, y, tileWidth, tileHeight, 30, 30);

        if (tile.getValue() != 0) {
            gc.setFill(getTileTextColor(tile));
            gc.setFont(getTileFont(tile));
            Bounds tileTextBounds = getTileTextBounds(tile);
            double xOffset = (tileWidth - tileTextBounds.getWidth()) / 2;
            double yOffset = (tileHeight + tileTextBounds.getHeight()) / 2 - tileTextBounds.getHeight() / 6;
            gc.fillText(tile.toString(), x + xOffset, y + yOffset);
        }
    }

    /**
     * Draw end game message on board
     *
     * @param text end game text
     */
    private void drawEndGameText(String text) {
        Font font = Font.font(TILE_FONT_NAME, FontWeight.BOLD, 62);
        GraphicsContext gc = getGraphicsContext2D();
        gc.setFont(font);
        Bounds textBounds = getTextBounds(font, text);
        double x = (getWidth() - textBounds.getWidth()) / 2;
        double y = (getHeight() + textBounds.getHeight()) / 2;
        gc.setFill(Color.BROWN);
        gc.fillRoundRect(x,
                y - 8 * textBounds.getHeight() / 10,
                textBounds.getWidth(),
                textBounds.getHeight(),
                30,
                30);
        gc.setFill(Color.YELLOW);
        gc.fillText(text, x, y);
    }

    /**
     * Board redraw method
     */
    public void draw() {
        Tile[][] gameTiles = controller.getGameTiles();

        GraphicsContext gc = getGraphicsContext2D();
        gc.setFill(BOARD_COLOR);
        gc.fillRect(0, 0, getWidth(), getHeight());
        for (int row = 0; row < TILE_NUMBER; row++)
            for (int column = 0; column < TILE_NUMBER; column++)
                drawTile(gameTiles[row][column], row, column);

        if (!controller.getModel().canMove())
            if (controller.getModel().isWin())
                drawEndGameText(winTextProperty.getValue());
            else
                drawEndGameText(loseTextProperty.getValue());
    }
}
