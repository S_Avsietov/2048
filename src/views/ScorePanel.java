package views;

import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import utils.Helper;


public class ScorePanel extends GridPane {
    /**
     * Percentage settings for columns
     */
    private static final int[] COLUMNS = new int[]{30, 5, 25, 40};
    /**
     * Percentage settings for rows
     */
    private static final int[] ROWS = new int[]{50, 50};

    private Label gameLogo;
    private Label scoreLabel;
    private Label scoreValue;

    public ScorePanel() {
        super();
        init();
    }

    /**
     * Initiation method for ScorePanel
     */
    private void init() {
        setStyle(Helper.PANEL_STYLE);
        gameLogo = Helper.createLabel("2048");
        scoreLabel = Helper.createBindedLabel("label.score");
        scoreValue = Helper.createLabel("0");
        Helper.markupGrid(this, COLUMNS, ROWS);
        add(gameLogo, 0, 0, 1, 2);
        add(scoreLabel, 2, 0, 2, 1);
        add(scoreValue, 2, 1, 2, 1);
    }


    public void setScore(String score) {
        scoreValue.setText(String.format(score));
    }
}
