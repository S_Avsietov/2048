package views;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import utils.Helper;

public class BackButtonPanel extends GridPane {
    /**
     * Percentage settings for columns
     */
    private static final int[] COLUMNS = new int[]{10, 80, 10};
    /**
     * Percentage settings for rows
     */
    private static final int[] ROWS = new int[]{100};

    private Button backButton;

    public BackButtonPanel() {
        init();
    }

    private void init() {
        setStyle(Helper.PANEL_STYLE);
        backButton = Helper.createBindedButton("button.back");
        setMargin(backButton,new Insets(10,10,10,10));
        Helper.markupGrid(this, COLUMNS, ROWS);
        add(backButton, 1, 0);
    }

    public void setOnBackPressed(EventHandler<ActionEvent> value) {
        backButton.setOnAction(value);
    }
}
