package views;

import javafx.scene.layout.GridPane;
import models.HighScoreTable;
import utils.Helper;

import java.io.IOException;
import java.time.format.DateTimeFormatter;

public class HighScoreTablePanel extends GridPane {
    /**
     * Percentage settings for columns
     */
    private static final int[] COLUMNS = new int[]{15, 55, 30};
    /**
     * Percentage settings for rows
     */
    private static final int[] ROWS = new int[]{10, 10, 10, 10, 10, 10, 10, 10, 10, 10};

    public HighScoreTablePanel() throws IOException, ClassNotFoundException {
        super();
        init();
    }

    private void init() throws IOException, ClassNotFoundException {
        setStyle(Helper.PANEL_STYLE);
        Helper.markupGrid(this, COLUMNS, ROWS);
        HighScoreTable highScoreTable = HighScoreTable.getInstance();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");
        for (int i = 0; i < ROWS.length; i++) {
            add(Helper.createSmallLabel(String.valueOf(i + 1)), 0, i);
            add(Helper.createSmallLabel(highScoreTable.
                    getHighScores()[i].getDateTime().format(formatter)), 1, i);
            add(Helper.createSmallLabel(
                    String.
                            valueOf(
                                    highScoreTable.
                                            getHighScores()[i].
                                            getPlayerScore()
                            )
            ), 2, i);
        }
    }
}
