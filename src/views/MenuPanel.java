package views;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import utils.Helper;

public class MenuPanel extends GridPane {

    /**
     * Percentage settings for columns
     */
    private static final int[] COLUMNS = new int[]{10, 80, 10};
    /**
     * Percentage settings for rows
     */
    private static final int[] ROWS = new int[]{10, 10, 10, 10, 10};

    private Button newGameButton;
    private Button saveButton;
    private Button loadButton;
    private Button langSelectButton;
    private Button highScoreTableButton;

    private static final String NEW_GAME_BUTTON_KEY = "button.newGame";
    private static final String SAVE_GAME_BUTTON_KEY = "button.saveGame";
    private static final String LOAD_GAME_BUTTON_KEY = "button.loadGame";
    private static final String CHANGE_LANG_BUTTON_KEY = "button.changeLang";
    private static final String HIGH_SCORE_TABLE_BUTTON_KEY = "button.highScoreTable";

    public MenuPanel() {
        super();
        init();
    }

    private void init() {
        setStyle(Helper.PANEL_STYLE);
        newGameButton = Helper.createBindedButton(NEW_GAME_BUTTON_KEY);
        saveButton = Helper.createBindedButton(SAVE_GAME_BUTTON_KEY);
        loadButton = Helper.createBindedButton(LOAD_GAME_BUTTON_KEY);
        langSelectButton = Helper.createBindedButton(CHANGE_LANG_BUTTON_KEY);
        highScoreTableButton = Helper.createBindedButton(HIGH_SCORE_TABLE_BUTTON_KEY);
        Helper.markupGrid(this, COLUMNS, ROWS);
        add(newGameButton, 1, 0);
        add(saveButton, 1, 1);
        add(loadButton, 1, 2);
        add(highScoreTableButton, 1, 3);
        add(langSelectButton, 1, 4);
    }

    public void setOnNewGameButtonAction(EventHandler<ActionEvent> value) {
        newGameButton.setOnAction(value);
    }

    public void setOnSaveButtonAction(EventHandler<ActionEvent> value) {
        saveButton.setOnAction(value);
    }

    public void setOnLoadButtonAction(EventHandler<ActionEvent> value) {
        loadButton.setOnAction(value);
    }

    public void setOnChangeLangButtonAction(EventHandler<ActionEvent> value) {
        langSelectButton.setOnAction(value);
    }

    public void setOnHighScoreTableButtonAction(EventHandler<ActionEvent> value) {
        highScoreTableButton.setOnAction(value);
    }

}
